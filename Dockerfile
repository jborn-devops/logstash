FROM docker.elastic.co/logstash/logstash:7.7.0

ARG ELK_PASSWORD

ENV LOGSTASH_KEYSTORE_PASS=changeme
RUN bin/logstash-keystore create
RUN echo 'elastic' | bin/logstash-keystore add ELASTIC_USER
RUN echo 'http://elasticsearch:9200' | bin/logstash-keystore add ELASTIC_URLS
RUN echo $ELK_PASSWORD | bin/logstash-keystore add ELASTIC_PASSWORD

RUN rm /usr/share/logstash/pipeline/logstash.conf
COPY logstash.yml /usr/share/logstash/config/logstash.yml
COPY conf/beats.conf /usr/share/logstash/pipeline/beats.conf

RUN logstash-plugin install logstash-input-beats
RUN logstash-plugin install logstash-filter-json